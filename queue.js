let collection = [];
// Write the queue functions below.

function enqueue(element) {
    collection[collection.length] = element;
    console.log(collection);
}

function print() {
    //return collection;
    console.log(collection);
}

function dequeue() {
    collection[0] = collection[1];
    collection.length = collection.length - 1;
}

function front() {
    console.log(collection[0]);
}

function size() {
    console.log(collection.length);
}

function isEmpty() {
    return (collection.length <= 0);
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
